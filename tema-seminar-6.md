# Codul morse
traduceti propozitia scrisa de utilizator, in codul morse.

pentru verificare folositi: http://morsecode.scphillips.com/translator.html (copy -> paste la codul morse si apasati play)

Daca utilizatorul introduce caractere nepermise (care nu se afla in lista de mai jos) atunci vom afisa un mesaj de eroare si nu vom oferi nici o traducere.


```
string[] morseCode = new string[40];
morseCode[0] = ".-";
morseCode[1] = "-...";
morseCode[2] = "-.-.";
morseCode[3] = "-..";
morseCode[4] = ".";
morseCode[5] = "..-.";
morseCode[6] = "--.";
morseCode[7] = "....";
morseCode[8] = "..";
morseCode[9] = ".---";
morseCode[10] = "-.-";
morseCode[11] = ".-..";
morseCode[12] = "--";
morseCode[13] = "-.";
morseCode[14] = "---";
morseCode[15] = ".--.";
morseCode[16] = "--.-";
morseCode[17] = ".-.";
morseCode[18] = "...";
morseCode[19] = "-";
morseCode[20] = "..-";
morseCode[21] = "...-";
morseCode[22] = ".--";
morseCode[23] = "-..-";
morseCode[24] = "-.--";
morseCode[25] = "--..";
morseCode[26] = ".----";
morseCode[27] = "..---";
morseCode[28] = "...--";
morseCode[29] = "....-";
morseCode[30] = ".....";
morseCode[31] = "-....";
morseCode[32] = "--...";
morseCode[33] = "---..";
morseCode[34] = "----.";
morseCode[35] = "-----";
morseCode[36] = "--..--";
morseCode[37] = ".-.-.-";
morseCode[38] = "..--..";
morseCode[39] = " / ";

string[] letters = new string[40];
letters[0] = "A";
letters[1] = "B";
letters[2] = "C";
letters[3] = "D";
letters[4] = "E";
letters[5] = "F";
letters[6] = "G";
letters[7] = "H";
letters[8] = "I";
letters[9] = "J";
letters[10] = "K";
letters[11] = "L";
letters[12] = "M";
letters[13] = "N";
letters[14] = "O";
letters[15] = "P";
letters[16] = "Q";
letters[17] = "R";
letters[18] = "S";
letters[19] = "T";
letters[20] = "U";
letters[21] = "V";
letters[22] = "W";
letters[23] = "X";
letters[24] = "Y";
letters[25] = "Z";
letters[26] = "1";
letters[27] = "2";
letters[28] = "3";
letters[29] = "4";
letters[30] = "5";
letters[31] = "6";
letters[32] = "7";
letters[33] = "8";
letters[34] = "9";
letters[35] = "0";
letters[36] = ",";
letters[37] = ".";
letters[38] = "?";
letters[39] = " ";
```