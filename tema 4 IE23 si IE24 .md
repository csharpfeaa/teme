# 1. Vectori uniti

Creati 2 vectori, cu dimensiunea `m` pentru primul vector si `n` pentru al doilea vector

- m, n le preluati de la utilizator
- populati cu numere random vectorii
- sortati vectorii

Unitii vectorii sortati, astfel incat numerele sa fie in ordine crescatoare. Veti avea un nou vector v[m+n]

Unirea vectorilor trebuie sa fie facuta fara sa necesite inca o sortare dupa unire.

# 2.Persistenta

Calculati persistenta unui numar, dupa urmatorul algoritm:

- spargeti numarul in cifre, si calculati produsul lor
- produsul va inlocui numarul initial
- repetati procesul pana cand ajungeti la o cifra
- numarul de pasi necesari pentru a reduce numarul la o cifra va reprezenta `persistenta`

Implementarea o veti face intr-o metoda `Persistenta` care va intoarce un numar reprezentand numarul de pasi necesari.

### Exemplu

Persistenta(39) = 3
pentru ca: 3*9 = 27, 2*7 = 14, 1*4=4 iar 4 are doar o cifra


Persistenta(999) = 4
pentru ca 9*9*9 = 729, 7*2*9 = 126, 1*2*6 = 12, iar in final 1*2 = 2


# 3. Asediu
Avand un numar de persoane asezati intr-un cerc, aflati ultima persoana care ramane dupa ce fiecare a 3 persoana este eliminata din cerc.

`Supravietuitorul(7,3)` => inseamna 7 persoane intr-un cerc, a 3 va fi eliminata

- [1,2,3,4,5,6,7] - secventa initiala
- [1,2,4,5,6,7] => 3 este eliminat
- [1,2,4,5,7] => 6 este eliminat
- [1,4,5,7] => 2 este eliminat
- [1,4,5] => 7 este eliminat
- [1,4] => 5 este eliminat
- [4] => 1 este eliminat, 4 ramane - supravietuitorul!



## ! Termen limita pentru trimiterea temei, pana la urmatorul seminar:

- IE24: 25 Noiembrie
- IE23: 28 Noiembrie

