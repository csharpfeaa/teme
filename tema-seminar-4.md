# 1. Ghiceste numarul
Ghiciti numarul random ales de calculator

Cerinte

* numarul aleator sa fie de la 0 la 100
* cereti un numar de la ultilizator pana cand ghiceste numarul ales
* dupa ce a ghicit numarul, afisati un mesaj de succes si numarul de incercari 

* pentru fiecare incercare afisati cat de aproape este:
    * <=3 - foarte fierbinte
    * <= 5 - fierbinte
    * <= 10 - cald
    * <= 20 - caldut
    * <= 50 - rece
    * \> 50 - foarte rece
    
Pentru a genera un numar aleator, puteti folosi:

```csharp
Random rand = new Random();

// metoda Next accepta un numar intreg, care este limita superioara pentru numarul generat
// in cazul acesta, se va genera un numar intre [0 si 10)
int numarAleator = rand.Next(10);
```


# 2. Suma multiplilor

Aflati suma multiplilor lui n mai mici decat m

* n, m sunt preluati de la utilizator
* n < m
* n,m numere naturale
* m este exclus de la multipli
* algoritmul de calcul va fi implementat intr-o metoda, ex: SumaMul


Exemplu:

```csharp
SumaMul(2,9) = 20: 2+4+6+8  
SumaMul(3,13) = 30
```